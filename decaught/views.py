from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render
from rest_framework import viewsets, status
from rest_framework.decorators import permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from .models import Device, Location, Geofence, Contact
from .serializers import DeviceSerializer, UserSerializer, LocationSerializer, GeofenceSerializer, ContactSerializer


def login(request):
    return render(request, 'login/index.html')


@login_required(login_url=settings.LOGIN_URL)
def main(request):
    devices = Device.objects.filter(user=request.user.pk, role='E')
    locations = []
    geofences = []

    for device in devices:
        locations.append(Location.objects.filter(device=device).order_by('-data_time').first())
        geofences.append(Geofence.objects.filter(device=device).first())

    context = {
        'device': devices,
        'location': locations,
        'geofence': geofences}

    return render(request, 'main/index.html', context)


@permission_classes((AllowAny,))
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class DeviceViewSet(viewsets.ModelViewSet):
    queryset = Device.objects.all()
    serializer_class = DeviceSerializer

    def list(self, request, *args, **kwargs):
        try:
            devices = Device.objects.filter(user=request.user.pk, role='E')
            serializer = DeviceSerializer(devices, many=True)
            response = Response(serializer.data)
        except Device.DoesNotExist:
            response = Response(status=status.HTTP_404_NOT_FOUND)
        return response

    def create(self, request, *args, **kwargs):
        device = Device.objects.filter(user=request.user.pk, device_id=request.data['imei'])
        if device:
            device.delete()
        if request.data['role'] == 'M':
            device = Device.objects.filter(user=request.user.pk, role='M')
            if device:
                device.delete()
        data = {'registration_id': request.data['regId'],
                'user': request.user.pk,
                'device_id': request.data['imei'],
                'type': 'android',
                'label': request.data['label'],
                'role': request.data['role'],
                'active': True}
        serializer = DeviceSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            response = Response(serializer.data)
        else:
            response = Response(serializer.errors)
        return response

    def put(self, request):
        try:
            device = Device.objects.get(device_id=request.data['imei'])
            device.registration_id = request.data['regId']
            device.save()
            serializer = DeviceSerializer(device)
            response = Response(serializer.data, status=status.HTTP_200_OK)
        except Device.DoesNotExist:
            response = Response(status=status.HTTP_404_NOT_FOUND)
        return response


class LocationViewSet(viewsets.ModelViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer

    def create(self, request, *args, **kwargs):
        try:
            device = Device.objects.get(user=request.user.pk, device_id=request.data['device'])
            data = {'device': device,
                    'data_time': request.data['data_time'],
                    'latitude': request.data['latitude'],
                    'longitude': request.data['longitude'],
                    'im_out': request.data['im_out']}
            serializer = LocationSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                response = Response(serializer.data)
                in_out = request.data['im_out']
                if in_out == "true":
                    text = device.label + ' está fuera de las áreas seguras.'
                    device = Device.objects.get(user=request.user, role='M')
                    device.send_message("Alerta de ubicación", text, data={'device': request.data['device']})
            else:
                response = Response(serializer.errors)
        except Device.DoesNotExist:
            response = Response(status=status.HTTP_404_NOT_FOUND)
        return response

    def list(self, request, *args, **kwargs):
        try:
            device = Device.objects.get(user=request.user.pk, device_id=request.GET['device'])
            location = Location.objects.filter(device=device).order_by('-data_time').first()
            resp = {'device': device.label, 'latitude': location.latitude, 'longitude': location.longitude}
            response = Response(resp)
        except Device.DoesNotExist:
            response = Response(status=status.HTTP_404_NOT_FOUND)
        return response


class GeofenceViewSet(viewsets.ModelViewSet):
    queryset = Geofence.objects.all()
    serializer_class = GeofenceSerializer

    def create(self, request, *args, **kwargs):
        try:
            device = Device.objects.get(user=request.user.pk, device_id=request.data['device'])
            data = {'device': device,
                    'latitude': request.data['latitude'],
                    'longitude': request.data['longitude'],
                    'radius': request.data['radius']}
            serializer = GeofenceSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                response = Response(serializer.data)
                device.send_message('cosa', 'cosa', data={'action': 'update_geofences'})
            else:
                response = Response(serializer.errors)
        except Device.DoesNotExist:
            response = Response(status=status.HTTP_404_NOT_FOUND)
        return response

    def list(self, request, *args, **kwargs):
        try:
            device = Device.objects.get(user=request.user.pk, device_id=request.GET['device'])
            geofences = Geofence.objects.filter(device=device)
            serializer = GeofenceSerializer(geofences, many=True)
            response = Response(serializer.data)
        except Device.DoesNotExist:
            response = Response(status=status.HTTP_404_NOT_FOUND)
        return response


class ContactViewSet(viewsets.ModelViewSet):
    queryset = Contact.objects.all()
    serializer_class = ContactSerializer

    def create(self, request, *args, **kwargs):
        try:
            device = Device.objects.get(user=request.user.pk, device_id=request.data['device'])
            data = {'device': device,
                    'phone': request.data['phone'],
                    'name': request.data['name']}
            serializer = ContactSerializer(data=data)
            if serializer.is_valid():
                serializer.save()
                response = Response(serializer.data)
            else:
                response = Response(serializer.errors)
        except Device.DoesNotExist:
            response = Response(status=status.HTTP_404_NOT_FOUND)
        return response

    def list(self, request, *args, **kwargs):
        try:
            devices = Device.objects.filter(user=request.user.pk, role=request.GET['role'])
            data = []
            for device in devices:
                contact = Contact.objects.get(device=device)
                data.append({'device': device.device_id,
                             'phone': contact.phone,
                             'name': contact.name})
            response = Response(data)
        except Device.DoesNotExist:
            response = Response(status=status.HTTP_404_NOT_FOUND)
        return response
