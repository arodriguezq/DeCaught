from django.contrib import admin
from .models import Device, Location, Geofence, Contact


# Register your models here.
class DeviceModelAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'label', 'device_id', 'type', 'active', 'role']
    exclude = ['name', 'type']


class LocationModelAdmin(admin.ModelAdmin):
    list_display = ['id', 'device', 'data_time', 'latitude', 'longitude', 'im_out']


class GeofenceModelAdmin(admin.ModelAdmin):
    list_display = ['id', 'device', 'latitude', 'longitude', 'radius']


class ContactModelAdmin(admin.ModelAdmin):
    list_display = ['id', 'device', 'phone', 'name']


admin.site.register(Device, DeviceModelAdmin)
admin.site.register(Location, LocationModelAdmin)
admin.site.register(Geofence, GeofenceModelAdmin)
admin.site.register(Contact, ContactModelAdmin)
