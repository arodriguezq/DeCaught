from django.contrib.auth.models import User
from django.db import models
from fcm_django.models import FCMDevice


class Device(FCMDevice):
    label = models.CharField(max_length=45, blank=False, null=False, verbose_name='Descripción')
    role = models.CharField(verbose_name='Rol del dispositivo', max_length=1, choices=(('E', 'Emisor'),
                                                                                       ('M', 'Maestro')))

    class Meta:
        verbose_name = 'Dispositivo'
        verbose_name_plural = 'Dispositivos'

    def __str__(self):
        return self.label
