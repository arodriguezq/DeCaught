from django.db import models

from decaught.entities.device import Device


class Contact(models.Model):
    device = models.ForeignKey(Device, null=False, on_delete=models.CASCADE, verbose_name='Dispositivo')
    phone = models.CharField(null=False, blank=False, max_length=15, verbose_name='Num. teléfono')
    name = models.CharField(null=False, blank=False, max_length=70, verbose_name='Nombre')

    class Meta:
        verbose_name = 'Contacto'
        verbose_name_plural = 'Contactos'

    def __str__(self):
        return self.name
