from django.db import models

from .device import Device


class Location(models.Model):
    device = models.ForeignKey(Device, null=False, on_delete=models.CASCADE, verbose_name='Dispositivo')
    data_time = models.DateTimeField(blank=False, null=False, verbose_name='Fecha y hora')
    latitude = models.DecimalField(max_digits=25, decimal_places=20, blank=False, null=False, verbose_name='Latitud')
    longitude = models.DecimalField(max_digits=25, decimal_places=20, blank=False, null=False, verbose_name='Longitud')
    im_out = models.BooleanField(default=False, verbose_name='Fuera')

    class Meta:
        verbose_name = 'Ubicación'
        verbose_name_plural = 'Ubicaciones'


class Geofence(models.Model):
    device = models.ForeignKey(Device, null=False, on_delete=models.CASCADE, verbose_name='Dispositivo')
    latitude = models.DecimalField(max_digits=25, decimal_places=20, blank=False, null=False, verbose_name='Latitud')
    longitude = models.DecimalField(max_digits=25, decimal_places=20, blank=False, null=False, verbose_name='Longitud')
    radius = models.FloatField(blank=False, null=False, default=600.0, verbose_name='Radio en metros')

    class Meta:
        verbose_name = 'Área segura'
        verbose_name_plural = 'Áreas seguras'
