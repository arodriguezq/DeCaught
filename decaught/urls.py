from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from decaught import views

urlpatterns = [
    url(r'^devices/$', views.DeviceViewSet),

]

urlpatterns = format_suffix_patterns(urlpatterns)
