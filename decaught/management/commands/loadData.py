from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
import datetime

from decaught.models import Device, Location


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-s', '--stress', type=int, dest='stress')

    def handle(self, *args, **options):
        if 'stress' in options:
            user = User.objects.filter(username='stress_user').first()
            if user:
                user.delete()
            user = User(username='stress_user', password='stress_user')
            user.save()
            device = Device(owner=user, label='Lab device', IMEI=100000000000000, type='E')
            device.save()
            for num in range(0, options['stress']):
                time = datetime.datetime.now() + datetime.timedelta(seconds=num)
                location = Location(device=device, data_time=time, latitude=100, longitude=-100, im_out=False)
                location.save()
        else:
            print('adiós')
