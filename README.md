Installation:

    pip install django, djangorestframework, psycopg2
    
    python manage.py makemigrations (Only if 'migrations' folder is empty)
    python manage.py migrate
    python manage.py createsuperuser
    
    python manage.py runserver

http POST http://127.0.0.1:8000/api-auth/ username="root" password="jupiter01"

Run server allowing external connections:

    python manage.py runserver 0.0.0.0:8000
