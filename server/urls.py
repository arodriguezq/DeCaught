"""server URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from decaught import models
from rest_framework import routers
from rest_framework.authtoken import views as cosa
from decaught import views
from django.contrib.auth.views import login, password_reset, password_reset_done, password_reset_confirm, password_reset_complete

router = routers.DefaultRouter()
router.register(r'devices', views.DeviceViewSet)
router.register(r'users', views.UserViewSet)
router.register(r'locations', views.LocationViewSet)
router.register(r'geofences', views.GeofenceViewSet)
router.register(r'contacts', views.ContactViewSet)

urlpatterns = [
    url('login/', login, {'template_name': 'login/index.html'}, name='login'),
    url('main/', views.main, name='main'),
    url(r'^admin/', admin.site.urls),
    url(r'^', include(router.urls)),
    url(r'^api-auth/', cosa.obtain_auth_token),
    url('reset/password_reset', password_reset,
        {'template_name': 'forget_pwd/password_reset_form.html',
         'email_template_name': 'forget_pwd/password_reset_email.html'},
        name='password_reset'),
    url('reset/password_reset_done', password_reset_done,
        {'template_name': 'forget_pwd/password_reset_done.html'},
        name='password_reset_done'),
    url('reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>.+)/$', password_reset_confirm,
        {'template_name': 'forget_pwd/password_reset_confirm.html'},
        name='password_reset_confirm'),
    url('reset/done', password_reset_complete,
        {'template_name': 'forget_pwd/password_reset_complete.html'},
        name='password_reset_complete'),
]

admin.site.site_header = 'Administración del proyecto de síntesis'
admin.site.index_title = 'Administración'
